import urllib.request, os

# Checks status code of URL held in lambda env var
def handler(message, context):
    url = os.environ['url']
   
    print(urllib.request.urlopen(url).getcode())
