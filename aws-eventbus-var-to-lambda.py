import json, os
from types import SimpleNamespace

def lambda_handler(event, context):

    # Receive json payload from event (i.e event bus)
    responsetype=json.dumps(event)
    data=json.loads(responsetype, object_hook=lambda d: SimpleNamespace(**d))
    
    # This prints the value of RESPONSE_TYPE to Cloudwatch Logs
    x = (data.detail.RESPONSE_TYPE)
    
    if x == "OK":
        print ("Status code = 200")
    else:
        raise Exception("Status code = 500", x) # If value isn't OK then throw exception

